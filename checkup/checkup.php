#!/usr/bin/php
<?php

    /*if(count($argv) == 1)
        return print_r('Visto che sei scemo ti faccio vedere come si usa'."\r\n"."\r\n"
                .'Specifica il nome della cartella del progetto dentro /var/www/html/ o il path assoluto'."\r\n"
                .'e.g. "php ck_updates.php Assiprest"'."\r\n"
                .'e.g. "php ck_updates.php /var/www/progetti/tipocosi/ldv'."\r\n");
//                .'e.g. "php ck_updates.php vesta log (mostra il verbose del processo)'."\r\n");

    $www = '/var/www/html/'.$argv[1];*/

    $log = false;
    if(isset($argv[1]) && ($argv[1] == 'log' || $argv[1] == 'logs'))
        $logs = true;

    exec('git rev-parse --git-dir 2> /dev/null', $out);

    if(isset($out[0]) && $out[0] == '.git') {
        exec('git branch', $branch);
        $branch = explode('* ', $branch[0])[1];
        print_r("Sei su ".$branch."\r\n");
        /*exec('git --no-pager diff --name-only HEAD $(git merge-base HEAD '.$branch.') 2> /dev/null', $output, $result);
        if(count($output) == 0)
            return print_r('Tutto apposto non devi fare niente'."\r\n");*/

        exec('git pull', $output, $result);

        $countSeeds = 0;
        $countMigrates = 0;
        $composer = false;
        $npm = false;

        //migrates
        foreach($output as $o) {
            if(strpos($o, '_table.php') || //non metto && perchè qualcuno potrebbe chiamare le migrate in maniera sbagliata
                (
                    strpos($o, 'create_') ||
                    strpos($o, 'alter_') ||
                    strpos($o, 'delete_')
                )
            )
                $countMigrates++;
        }

        if($countMigrates > 0)
            $logs == false ? exec('php artisan migrate 2> /dev/null 2>&1 &') : exec('php artisan migrate');

        //seeders
        foreach($output as $o) {
            if(strpos($o, 'database/seeds/')) {
               $seed = preg_split('([A-Z])\w+.php', $o); //trova tutte le occorrenze con *.php
               $seed = substr($seed[count($seed)-1], 0, strlen($seed[count($seed)-1])-4)."\r\n";
               $logs == false ? exec('php artisan db:seed --class='.$seed.' 2> /dev/null 2>&1 &') : exec('php artisan db:seed --class='.$seed);
               $countSeeds++;
            }

            if(strpos($o, 'composer.json'))
                $composer = true;

            if(strpos($o, 'package.json'))
                $npm = true;
        }

        //aggiornamento pacchetti angular
        if($npm)
            $logs == false ? exec('npm install 2> /dev/null 2>&1 &') : exec('npm install');

        if($composer)
            $logs == false ? exec('composer update 2> /dev/null 2>&1 &') : exec('composer update');

        print_r("\r\n".'+------ All Right ------+'."\r\n");
        print_r('I Did:'."\r\n");
        print_r('[-] Migrate: '.$countMigrates."\r\n");
        print_r('[-] Seeders: '.$countSeeds."\r\n");
        $composer ? print_r('[-] Updated composer packets'."\r\n") : print_r('');
        $npm ? print_r('[-] Updated node packets'."\r\n") : print_r('');
    }
    else
        print_r('Not a git repository!'."\r\n");

?>
