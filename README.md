<h1>About</h1>
<b>A simple php script that automatically git pulls and launch the following laravel/npm commands:</b>
<ul>
    <li><b>Migrating</b></li>
    <li><b>Seeding new/modified seeders</b></li>
    <li><b>Updating composer packages</b></li>
    <li><b>Updating node.js packages</b></li>
</ul>


<h1>Installation</h1>

<h5>Download the project and put the <code>checkup</code> directory in <code>/etc</code></h5>

<h5>Edit <code>./bashrc</code> (normally located in <code>/home/$USER</code>) and append to the end of file the following code:<br>

<b><code>alias checkup="/etc/checkup/.checkup.sh"</code><b>

<b>Then, reload the bash configuration file by typing:</b> 

<b><code>source ~/.bashrc</code></b></h5>


<h1>Usage</h1>

<h5>Just type <code>checkup</code> in a git repository</h5>
<h5>Use <code>checkup log</code> to show process verbose</h5>
